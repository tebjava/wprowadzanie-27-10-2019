import java.util.Scanner;

/*
 * To jest komentarz wieloliniowy
 * czyli taki, kt�ry mo�e zajac kilka/kilkana�cie linii w naszym kodzie zr�d�owym
 * Tekst zapisany w komentarzu nie jest brany pod uwage przez kompilator/parser 
 */
// przyk�ad komentarza jednoliniowego; mo�na go rozpoczac w dowolnym momencie jednak konczy sie on wraz z ko�cem linii, w kt�rym zosta� napisany
// przez co ka�dorazowo musimy go "otwierac" (napisac sekwencje //)
public class Podstawy {

	public static void main(String[] args) {
		
		/*
		 * Poni�ej zaprezentowane zosta�y zmienne proste (primitives). Zajmuja one 
		 * zawsze okre�lona ilosc miejsca w pamieci RAM i nie posiadaja one
		 * zadnych dodatkowych cech, funkcji czy mozliwosci bezposrednich dzialan
		 * na obiektach
		 */
		
		//zmienna typu prawda/fa�sz. Przyjmuje tylko dwie warto�ci:
		// fa�sz (false, binarnie 0) - najczesciej wykorzystuje sie jako oznajmnienie, �e
		//                             co� nie wystepuje/zako�czy�o sie inaczej niz przewidzielismy
		// prawda (true, binarnie 1) - najczesciej wykorzystuje sie jako oznajmienie, �e
	    //							  co� wystepuje/wykona�o sie poprawnie
		boolean pf = true;
		//zmienna liczbowa - liczba ca�kowita. Mo�e przyjmowac wartosci zapisywane 
		//na 4 bajtach (32 bity, ok. 4,4 mld, dzielone na polowe - wartosci ujemne i dodatnie).
		//zmienna tego typu pozwala na dzialania matematyczne (artymetyka, trygonometria itp.)
		int liczba = 156;
		//zmienna liczowa - liczba ca�kowita kr�tka (po�owa warto�ci int - 16 bit/2bajty)
		short liczbaKrotka = -15;
		//zmienna liczbowa - liczba ca�kowita dluga (dwukrtonosc int)
		long liczbaDluga = 16789;
		//zmienna liczbowa - liczba zmiennoprzecinkowa (floating point). Mo�emy zapisywac 
		//liczby u�amkowe na 6-7 miejsc po przecinku (ca�osc na 4 bajtach)
		float zm = 1.15f;
		//zmienna liczbowa - liczba zmiennoprzecinkowa PODWOJNEJ precyzji (wiekszy zakres 
		//dla wartosci u�amkowej - 15-16 miejsc po przecinku (ca�osc na 8 bajtach)
		double zm2 = -1.15;
		//zmienna znakowa - pozwala na przechowanie pojedynczego znaku z tablicy ASCII
		//zmienna tak naprawde zawiera liczbe odpowiadajaca kodowi w ASCII
		//zajmuje 1 bajt 
		char znak='A';
		//typ tzw. alias (inna postac char) pozwalajacy na zapis wartosci na jednym bajcie
		byte b = 'n';
		
		/*
		 * Zmienne zlozone - najczesciej tworzone z klas (sa obiektami). Nie zajmuja okreslonej
		 * przestrzeni w pamieci RAM (moga nie zajmowac nic, moga zajmowac nawet kilka/
		 * kilkanascie GB pamieci). Czesto moga posiadac dodatkowa funkcjonalnosc oraz
		 * moga byc zamieniane na inne typy zlozone.
		 */
		
		//zmienna typu znakowego. Pozwala na przechowywanie dowolnej ilosci znakow
		//(ograniczenie wynika ewentualnie jednie ze sepecyfikacji JVM, systemu opracyjnego
		//badz ograniczen sprzetowych)
		String ciagZnakowy = "Dobry jezyk!";
		
		
		
		
		// TODO Auto-generated method stub
		System.out.println("Witaj w �wiecie Java!");
		System.out.print("Tekst z nastepnej linii");
		Scanner s = new Scanner(System.in);
		
		//int liczba1= s.nextInt();
		int liczba2=s.nextInt();
	}

}
